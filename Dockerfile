FROM alpine:edge

LABEL maintainer="Andreas Peters <support@aventer.biz>"

ARG DOMAIN "localhost:8080"

COPY ./mumble-web /home/node

RUN echo http://nl.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories && \
    echo http://dl-cdn.alpinelinux.org/alpine/v3.14/main >> /etc/apk/repositories && \
    apk add --no-cache git nodejs=14.20.1-r0 npm tini websockify openssl && \
    adduser -D -g 1001 -u 1001 -h /home/node node && \
    mkdir -p /home/node && \
    mkdir -p /home/node/.npm-global && \
    mkdir -p /home/node/app  && \
    chown -R node: /home/node 

RUN openssl req -new -x509 -days 365 -nodes -out /self.pem -keyout /self.pem -subj "/CN=${DOMAIN}" && chmod +r /self.pem

USER node

ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

RUN cd /home/node && \
    npm install && \
    npm run build 

USER root

RUN apk del gcc git

USER node

EXPOSE 8080
ENV MUMBLE_SERVER=mumble.aventer.biz:64738

ENTRYPOINT ["/sbin/tini", "--"]
CMD websockify --ssl-target --web=/home/node/dist 8080 "$MUMBLE_SERVER"

