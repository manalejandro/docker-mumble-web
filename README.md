# Docker Mumble Web

## Simple `docker compose` project to build and run two containers, [mumble-web](https://github.com/Johni0702/mumble-web) and [mumble-server](https://hub.docker.com/r/mumblevoip/mumble-server)

## Build

```
git clone --recurse-submodules https://git.manalejandro.com/ale/docker-mumble-web

Edit root Dockerfile and write your DOMAIN variable

$ docker-compose build && docker-compose pull
```

## Run

```
$ docker-compose up -d

Join to https://localhost:8080/

Mumble server config is in the local ./data folder
```

![login](image.png)

## License

```
MIT
```
